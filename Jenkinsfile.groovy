node {
    def mvnHome
    stage('BitBucket Pull') {
        //git 'https://nishadmehendale@bitbucket.org/nishadmehendale/mule-demo'
        git 'https://nishadmehendale@bitbucket.org/nishadmehendale/test.git'
        mvnHome = tool 'maven3'
    }

    stage('Build') {
        // Run the maven build
        sh "'${mvnHome}/bin/mvn' -Dmaven.test.failure.ignore clean package"
    }
    stage('Pushing to Artifactory'){
      withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId:"Artifactory", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
         sh"curl -X PUT -u $USERNAME:$PASSWORD -T ${workspace}/target/*.zip 'http://ec2-54-201-29-152.us-west-2.compute.amazonaws.com:8081/artifactory/libs-release-local/nishad/Mule.zip'"
      }
    }
    
    //stage('Deploying'){
        //withCredentials([sshUserPrivateKey(credentialsId: 'MULE_DEV_ACCESS'/*, keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: 'userName'*/)]) {
        //remote.user = ec2-user
        //remote.identityFile = MULE_DEV_ACCESS
            //ssh ec2-54-201-29-152.us-west-2.compute.amazonaws.com 'sudo su' -p 22
            //sshCommand remote: remote, command: 'wget http://ec2-54-201-29-152.us-west-2.compute.amazonaws.com:8081/artifactory/libs-release-local/nishad/Mule.zip'
            
        //}
        //sshagent (credentials: ['MULE_DEV_ACCESS']) {
        //            sh "ssh -o StrictHostKeyChecking=no -l ec2-user ec2-54-202-123-236.us-west-2.compute.amazonaws.com 'pwd'"
        //            sh "ssh -o StrictHostKeyChecking=no -l ec2-user ec2-54-202-123-236.us-west-2.compute.amazonaws.com 'rm *.zip -f'"
        //            sh "ssh -o StrictHostKeyChecking=no -l ec2-user ec2-54-202-123-236.us-west-2.compute.amazonaws.com 'sudo chmod -R -f 777 apps'"
        //            sh "ssh -o StrictHostKeyChecking=no -l ec2-user ec2-54-202-123-236.us-west-2.compute.amazonaws.com 'rm -f -R Mule'"
        //            sh "ssh -o StrictHostKeyChecking=no -l ec2-user ec2-54-202-123-236.us-west-2.compute.amazonaws.com 'wget http://ec2-54-201-29-152.us-west-2.compute.amazonaws.com:8081/artifactory/libs-release-local/nishad/Mule.zip -P ./apps/'"
        //            sh "ssh -o StrictHostKeyChecking=no -l ec2-user ec2-54-202-123-236.us-west-2.compute.amazonaws.com 'sudo chmod -R -f 664 apps'"
        //           sh "ssh -o StrictHostKeyChecking=no -l ec2-user ec2-54-202-123-236.us-west-2.compute.amazonaws.com 'ls -a'"
        //          }
    
    //}
}